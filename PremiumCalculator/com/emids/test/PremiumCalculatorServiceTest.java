package com.emids.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.emids.main.model.Person;
import com.emids.main.service.PremiumCalculatorService;

public class PremiumCalculatorServiceTest {

	Person per = new Person();

	@Before
	public void setUpData() {
		Map<String, String> healthMap = new HashMap<String, String>();
		Map<String, String> habitMap = new HashMap<String, String>();

		per.setName("Nil Sidai");
		per.setAge("25");
		per.setGender("Male");

		healthMap.put("Hypertension", "YES");
		healthMap.put("BloodPressure", "YES");
		healthMap.put("BloodSugar", "YES");
		healthMap.put("Overweight", "YES");

		per.setHealthData(healthMap);

		habitMap.put("Smoking", "YES");
		habitMap.put("Alcohol", "NO");
		habitMap.put("DailyExercise", "YES");
		habitMap.put("Drugs", "YES");

		per.setHabitData(habitMap);

	}

	@Test
	public void getPremiumForAgeTest() {
		int result = PremiumCalculatorService.getPremiumForAge(per);
		assertEquals(500, result);

	}

	@Test
	public void getPermiumForGenger() {
		int result = PremiumCalculatorService.getPermiumForGenger(per);
		assertEquals(100, result);

	}

	@Test
	public void getPremiunForHealth() {
		int result = PremiumCalculatorService.getPremiunForHealth(per);
		assertEquals(200, result);

	}

	@Test
	public void getPremiunForHabit() {
		int result = PremiumCalculatorService.getPremiunForHabit(per);
		assertEquals(150, result);

	}

}

package com.emids.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.emids.main.model.Person;
import com.emids.main.validator.PremiumCalculatorValidator;


public class PremiumCalculatorValidatorTest {

	Person per = new Person();
	
	@Before
	public void setUpData() {
		Map<String, String> healthMap = new HashMap<String, String>();
		Map<String, String> habitMap = new HashMap<String, String>();
		
		per.setName("Nil Sidai");
		per.setAge("25");
		per.setGender("Male");
		
		healthMap.put("Hypertension", "YES");
		healthMap.put("BloodPressure", "YES");
		healthMap.put("BloodSugar", "YES");
		healthMap.put("Overweight", "YES");
		
		per.setHealthData(healthMap);
		
		habitMap.put("Smoking", "YES");
		habitMap.put("Alcohol", "YES");
		habitMap.put("DailyExercise", "YES");
		habitMap.put("Drugs", "YES");
		
		per.setHabitData(habitMap);
		
	}
	
	@Test
	public void validatePerson() {
		assertTrue(PremiumCalculatorValidator.validatePerson(per));
	}
}

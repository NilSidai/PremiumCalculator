package com.emids.main.service;

import java.util.Map;

import com.emids.main.constants.PremiumConstants;
import com.emids.main.model.Person;

public class PremiumCalculatorService {

	static int premium = PremiumConstants.minimumPremium;

	public static Integer getTotalPremium(Person per) {

		Integer extraPremium = getPremiumForAge(per) + getPermiumForGenger(per)
				+ getPremiunForHealth(per) + getPremiunForHabit(per);

		return premium + extraPremium;
	}

	public static Integer getPremiumForAge(Person per) {

		int age = Integer.parseInt(per.getAge());
		int extraPremium = 0;
		if (age >= 18 && age <= 40) {
			extraPremium = (int) (premium * .1);
			return extraPremium;
		}
		if (age > 40) {
			int ageSlab = (age - 40) / 5;
			extraPremium = (int) (ageSlab * (premium * .2));
			return extraPremium;
		}
		return extraPremium;
	}

	public static Integer getPermiumForGenger(Person per) {
		int extraPremium = 0;
		if (per.getGender().equalsIgnoreCase("MALE")) {
			extraPremium = (int) (premium * .02);
		}
		return extraPremium;
	}

	public static Integer getPremiunForHealth(Person per) {
		int count = 0;
		int extraPremium = 0;
		for (Map.Entry<String, String> temp : per.getHealthData().entrySet()) {
			if (temp.getValue().equalsIgnoreCase("YES")) {
				count++;
			}
		}
		extraPremium = (int) (count * (premium * .01));
		return extraPremium;

	}

	@SuppressWarnings("unused")
	public static Integer getPremiunForHabit(Person per) {
		int count = 0;
		boolean reducePremiumFlag = false;
		int extraPremium = 0;
		for (Map.Entry<String, String> temp : per.getHabitData().entrySet()) {
			if (temp.getKey().equalsIgnoreCase("DailyExercise")) {
				reducePremiumFlag = true;
			} else if (temp.getValue().equalsIgnoreCase("YES")) {
				count++;
			}
		}

		extraPremium = (int) (count * (premium * .03));

		if (reducePremiumFlag = true) {
			extraPremium = extraPremium - (int) (premium * .03);
		}

		return extraPremium;
	}

}

package com.emids.main.model;

import java.util.Map;

public class Person {
	private String name;
	private String gender;
	private String age;
	private Map<String, String> healthData;
	private Map<String, String> habitData;

	public Person() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Map<String, String> getHealthData() {
		return healthData;
	}

	public void setHealthData(Map<String, String> healthData) {
		this.healthData = healthData;
	}

	public Map<String, String> getHabitData() {
		return habitData;
	}

	public void setHabitData(Map<String, String> habitData) {
		this.habitData = habitData;
	}

}

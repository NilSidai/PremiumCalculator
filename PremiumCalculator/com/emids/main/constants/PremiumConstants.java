package com.emids.main.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PremiumConstants {

	private static List<String> healthData = new ArrayList<String>(
			Arrays.asList("Hypertension", "BloodPressure", "BloodSugar",
					"Overweight"));

	private static List<String> habitData = new ArrayList<String>(
			Arrays.asList("Smoking", "Alcohol", "DailyExercise", "Drugs"));

	public static final Integer minimumPremium = 5000;

	public static List<String> getHealthData() {
		return healthData;
	}

	public static List<String> getHabitData() {
		return habitData;
	}

}

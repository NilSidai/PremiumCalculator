package com.emids.main.validator;

import java.util.Map;

import com.emids.main.model.Person;

public class PremiumCalculatorValidator {
	
	public static boolean validatePerson(Person per) {
		if (per.getName().isEmpty() || per.getName() == null)
			return false;
		else if (per.getName().length() < 5 || per.getName().length() > 30)
			return false;
		try {
			if (Integer.parseInt(per.getAge()) < 1
					|| Integer.parseInt(per.getAge()) > 125)
				return false;
		} catch (Exception ex) {
			return false;
		}
		for (Map.Entry<String, String> temp : per.getHealthData().entrySet()) {
			if (!(temp.getValue().equalsIgnoreCase("YES") || temp.getValue().equalsIgnoreCase("NO"))) {
				return false;
			}
		}
		for (Map.Entry<String, String> temp : per.getHabitData().entrySet()) {
			if (!(temp.getValue().equalsIgnoreCase("YES"))|| temp.getValue().equalsIgnoreCase("NO")) {
				return false;
			}
		}
		return true;
	}

}

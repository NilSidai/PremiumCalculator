package com.emids.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.emids.main.constants.PremiumConstants;
import com.emids.main.model.Person;
import com.emids.main.service.PremiumCalculatorService;
import com.emids.main.validator.PremiumCalculatorValidator;

public class PremiumCalculator {

	public static void main(String[] args) throws IOException {
		Person per = new Person();
		Map<String, String> healthMap = new HashMap<String, String>();
		Map<String, String> habitMap = new HashMap<String, String>();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out
				.println("Welcome to PremiumCulator!! Please provide your details.");
		System.out.println("Please enter your NAME.And Press ENTER");
		per.setName(br.readLine());

		System.out
				.println("Please enter your GENDER(MALE/FEMALE/OTHER).And Press ENTER");
		per.setGender(br.readLine());

		System.out.println("Please enter your AGE(1-125).And Press ENTER");
		per.setAge(br.readLine());

		for (String healthData : PremiumConstants.getHealthData()) {
			System.out.println("Do you have " + healthData + " (YES/NO)?");
			healthMap.put(healthData, br.readLine());
		}
		per.setHealthData(healthMap);

		for (String habitData : PremiumConstants.getHabitData()) {
			System.out.println("Do you have a habit of " + habitData
					+ " (YES/NO)?");
			habitMap.put(habitData, br.readLine());
		}
		per.setHabitData(habitMap);

		if (PremiumCalculatorValidator.validatePerson(per))
			System.out.println("Your total Premium is "
					+ PremiumCalculatorService.getTotalPremium(per));
		else
			System.out
					.println("The data provided by you is incorrect. Please try again.");

	}
	

}
